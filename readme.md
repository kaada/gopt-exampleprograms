# Description

This repository contains example programs for GoPT and Go2 which were made for my master's thesis.
Some aspects of the code are inspired from other programs, and are clearly marked with urls.
For the 'linked list'-files it is the structure of the linked list which is based on another program, namely 'list transform'-example from the url in the files.
For the 'graph'-files it is some methods, and is clearly marked.

## File description
### go2graph.go2
The complete example of the railway network example for Go2. 

### rtgraph.go2 
The full example of the railway network example for GoPT.

### go2linkedlist.go2
The full example of the audio player example for Go2.

### rtlinkedlist.go2
The full example of the audio player example for GoPT. 

### go2graph_diff.txt 
The 'git diff' between go2graph.go2 and a version which contains a new generic parameter 'NewType'.

### rtgraph_diff.txt
The 'git diff' between rtgraph.go2 and a version which contains a new generic parameter 'NewType'.
