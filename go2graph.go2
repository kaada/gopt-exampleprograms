package main

import (
	"fmt"
	"sort"
)


const Infinity = int(^uint(0) >> 1)

// start of "GraphTemplate"
type NodeItem interface {
	comparable // to be able to use it as key in map and to be sure to use "=="/"!="
	String() string
}

type EdgeCost interface {
	GetCost() int
}

type Node[T NodeItem] struct {
    item T
}

// inspired by https://flaviocopes.com/golang-data-structure-graph/
func (n *Node[T]) String() string {
	return fmt.Sprintf("%v", n.item.String())
}

type Edge[T NodeItem, C EdgeCost] struct {
	from *Node[T]
	to *Node[T]
	edgeCost C
}

type Graph[T NodeItem, C EdgeCost] struct { 
	nodes []*Node[T]
	edges map[Node[T]][]*Edge[T, C]
}

// inspired by https://flaviocopes.com/golang-data-structure-graph/
func (g *Graph[T, C]) AddNode(n *Node[T]){
	g.nodes = append(g.nodes, n)
}

// inspired by https://flaviocopes.com/golang-data-structure-graph/
func (g *Graph[T, C]) AddEdge(edgeFrom, edgeTo *Edge[T, C]){
	if g.edges == nil {
        g.edges = make(map[Node[T]][]*Edge[T, C])
    }
    g.edges[*(edgeFrom.from)] = append(g.edges[*(edgeFrom.from)], edgeFrom)
    g.edges[*(edgeTo.from)] = append(g.edges[*(edgeTo.from)], edgeTo)
}

// inspired by https://flaviocopes.com/golang-data-structure-graph/
func (g *Graph[T, C]) String() {
	s := ""
    for i := 0; i < len(g.nodes); i++ {
        s += g.nodes[i].String() + " -> "
        near := g.edges[*g.nodes[i]]
        for j := 0; j < len(near); j++ {
            s += near[j].to.String() + " "
        }
        s += "\n"
    }
    fmt.Println(s)
}

// inspired by https://github.com/golang/go/blob/dev.go2go/src/cmd/go2go/testdata/go2path/src/graph/graph.go2
type nodePath[T NodeItem, C EdgeCost] struct {
	node Node[T]
	nodesEdges []*Edge[T, C]
	path []Edge[T, C]
}

// inspired by https://github.com/golang/go/blob/dev.go2go/src/cmd/go2go/testdata/go2path/src/graph/graph.go2
func (g *Graph[T, C]) ShortestPath(from, to Node[T]) ([]Edge[T, C], bool) {
	visited := make(map[Node[T]]bool)
	visited[from] = true
	var workqueue []nodePath[T, C]
	workqueue = append(workqueue, nodePath[T, C]{node: from, nodesEdges: g.edges[from], path: nil})
	

	for len(workqueue) > 0 {
		current := workqueue
		workqueue = nil
		for _, np := range current {
			edges := np.nodesEdges
			for _, edge := range edges {
				a := *edge.from
				b := *edge.to
				if a == np.node {
					a = b
				}
				if !visited[a] {
					ve := append([]Edge[T, C](nil), np.path...)
					ve = append(ve, *edge)
					if a == to {
						return ve, true
					}
					workqueue = append(workqueue, nodePath[T, C]{node: a, nodesEdges: g.edges[a], path: ve})
					visited[a] = true
				}
			}
		}
	}
	
	return nil, false
}

// inspired by https://deployeveryday.com/2019/10/16/dijkstra-algorithm-golang.html
func (g *Graph[T, C]) Dijkstra(startNode *Node[T]) (shortestPathTable string) {
	costTable := g.NewCostTable(startNode)
	var visited []*Node[T]

	for len(visited) != len(g.nodes) {
		node := getClosestNonVisitedNode(costTable, visited)
		visited = append(visited, node)
		nodeEdges := g.edges[*node]

		for _, edge := range nodeEdges {
			distanceToNeighbor := costTable[node] + edge.edgeCost.GetCost()
			if distanceToNeighbor < costTable[edge.to] {
				costTable[edge.to] = distanceToNeighbor
			}
		}
	}

	for node, cost := range costTable {
		shortestPathTable += fmt.Sprintf("Distance from %s to %s = %d\n", startNode.String(), node.String(), cost)
	}

	return shortestPathTable
}

// inspired by https://deployeveryday.com/2019/10/16/dijkstra-algorithm-golang.html
func (g *Graph[T, C]) NewCostTable(startNode *Node[T]) map[*Node[T]]int {
	costTable := make(map[*Node[T]]int)
	costTable[startNode] = 0

	for _, node := range g.nodes {
		if node != startNode {
			costTable[node] = Infinity
		}
	}

	return costTable
}

// inspired by https://deployeveryday.com/2019/10/16/dijkstra-algorithm-golang.html
func getClosestNonVisitedNode[T NodeItem](costTable map[*Node[T]]int, visited []*Node[T]) *Node[T] {
	type CostTableToSort struct {
		Node *Node[T]
		Cost int
	}
	var sorted []CostTableToSort

	for node, cost := range costTable {
		var isVisited bool
		for _, visitedNode := range visited {
			if node == visitedNode {
				isVisited = true
			}
		}
		if !isVisited {
			sorted = append(sorted, CostTableToSort{Node: node, Cost: cost})
		}
	}

	sort.Slice(sorted, func(i, j int) bool {
		return sorted[i].Cost < sorted[j].Cost
	})

	return sorted[0].Node
}
// end of "GraphTemplate"

// start of "GeoNetwork" template
type GeoLocation interface {
	NodeItem
	GetCoordinates() (float32, float32)
}

type MeansOfTransport[T GeoLocation, C EdgeCost] interface {
	String() string
	TimeBetweenTwoPlaces(edge *Road[T, C]) int
}

// Graph renaming
type GeoNetwork[T GeoLocation, C EdgeCost] struct { 
	graph *Graph[T, C]
}

func (g *GeoNetwork[T, C]) CalculateShortestRoutes(startNode *Node[T]) (shortestPathTable string) {
	return g.graph.Dijkstra(startNode)
}

func (g *GeoNetwork[T, C]) AddRoad(fromEdge, toEdge *Road[T, C]) {
	g.graph.AddEdge(fromEdge.edge, toEdge.edge)
}

func (g *GeoNetwork[T, C]) AddPlace(n *Node[T]) {
	g.graph.AddNode(n)
}

type Road[T GeoLocation, C EdgeCost] struct {
	edge *Edge[T, C]
	meansOfTransport MeansOfTransport[T, C]
	km int
}

type TravelTime struct {
	cost int
}

func (t TravelTime) GetCost() int {
	return t.cost
}
// end of "GeoNetwork" template

// "outer-scope"
type Place struct {
	name string
	latitude float32
	longitude float32
}

func (c Place) String() string {
	return c.name
}

func (c Place) GetCoordinates() (float32, float32) {
	return c.latitude, c.longitude 
}

type Train[T GeoLocation, C EdgeCost] struct {
	name string
}

func(t Train[T, C]) String() string {
	return t.name
}

func (t Train[T, C]) TimeBetweenTwoPlaces(road *Road[T, C]) int {
	return (road.km)/t.AverageSpeed()
}

func (t Train[T, C]) AverageSpeed() int {
	return 83
}



type RailLine = Road
type RailMap = GeoNetwork[Place, TravelTime]
type TrainStation = Node

func RailMapTest(){
	var AddCity = (*RailMap).AddPlace
	var AddRailLine = (*RailMap).AddRoad
	
	var railMap RailMap
	railMap.graph = &Graph[Place, TravelTime]{}

	aberdeen := TrainStation[Place]{item: Place { name: "Aberdeen", latitude: 57.14, longitude: -2.10 } }
	brussel := TrainStation[Place]{item: Place { name: "Brussel", latitude: 50.51, longitude: 4.20 } }
	cannes := TrainStation[Place]{item: Place { name: "Cannes", latitude: 43.55, longitude: 7.01 } }
	dortmund := TrainStation[Place]{item: Place { name: "Dortmund", latitude: 51.51, longitude: 7.46 } } 
	elche := TrainStation[Place]{item: Place { name: "Elche", latitude: 38.26, longitude: -0.69} }
	fiorentina := TrainStation[Place]{item: Place { name: "Fiorentina", latitude: 43.46, longitude: 11.15} }
	AddCity(&railMap, &aberdeen)
	AddCity(&railMap, &brussel)
	AddCity(&railMap, &cannes)
	AddCity(&railMap, &dortmund)
	AddCity(&railMap, &elche)
	AddCity(&railMap, &fiorentina)

	aberdeenToBrussel := &RailLine[Place, TravelTime]{edge: &Edge[Place, TravelTime]{from: &aberdeen, to: &brussel}, meansOfTransport: &Train[Place, TravelTime]{"train"}, km: 1220.0}
	aberdeenToBrussel.edge.edgeCost = TravelTime{cost: aberdeenToBrussel.meansOfTransport.TimeBetweenTwoPlaces(aberdeenToBrussel)}
	brusselToAberdeen := &RailLine[Place, TravelTime]{edge: &Edge[Place, TravelTime]{from: &brussel, to: &aberdeen}, meansOfTransport: &Train[Place, TravelTime]{"train"}, km: 1220.0}
	brusselToAberdeen.edge.edgeCost = TravelTime{cost: brusselToAberdeen.meansOfTransport.TimeBetweenTwoPlaces(brusselToAberdeen)}
	
	aberdeenToCannes := &RailLine[Place, TravelTime]{edge: &Edge[Place, TravelTime]{from: &aberdeen, to: &cannes}, meansOfTransport: &Train[Place, TravelTime]{"train"}, km: 2259.0}
	aberdeenToCannes.edge.edgeCost = TravelTime{cost: aberdeenToCannes.meansOfTransport.TimeBetweenTwoPlaces(aberdeenToCannes)}
	cannesToAberdeen := &RailLine[Place, TravelTime]{edge: &Edge[Place, TravelTime]{from: &cannes, to: &aberdeen}, meansOfTransport: &Train[Place, TravelTime]{"train"}, km: 2259.0}
	cannesToAberdeen.edge.edgeCost = TravelTime{cost: cannesToAberdeen.meansOfTransport.TimeBetweenTwoPlaces(cannesToAberdeen)}
	
	brusselToElche := &RailLine[Place, TravelTime]{edge: &Edge[Place, TravelTime]{from: &brussel, to: &elche}, meansOfTransport: &Train[Place, TravelTime]{"train"}, km: 1886.0}
	brusselToElche.edge.edgeCost = TravelTime{cost: brusselToElche.meansOfTransport.TimeBetweenTwoPlaces(brusselToElche)}
	elcheToBrussel := &RailLine[Place, TravelTime]{edge: &Edge[Place, TravelTime]{from: &elche, to: &brussel}, meansOfTransport: &Train[Place, TravelTime]{"train"}, km: 1886.0}
	brusselToElche.edge.edgeCost = TravelTime{cost: elcheToBrussel.meansOfTransport.TimeBetweenTwoPlaces(elcheToBrussel)}

	cannesToElche := &RailLine[Place, TravelTime]{edge: &Edge[Place, TravelTime]{from: &cannes, to: &elche}, meansOfTransport: &Train[Place, TravelTime]{"train"}, km: 1159.0}
	cannesToElche.edge.edgeCost = TravelTime{cost: cannesToElche.meansOfTransport.TimeBetweenTwoPlaces(cannesToElche)}
	elcheToCannes := &RailLine[Place, TravelTime]{edge: &Edge[Place, TravelTime]{from: &elche, to: &cannes}, meansOfTransport: &Train[Place, TravelTime]{"train"}, km: 1159.0}
	elcheToCannes.edge.edgeCost = TravelTime{cost: elcheToCannes.meansOfTransport.TimeBetweenTwoPlaces(elcheToCannes)}
	
	elcheToFiorentina := &RailLine[Place, TravelTime]{edge: &Edge[Place, TravelTime]{from: &elche, to: &fiorentina}, meansOfTransport: &Train[Place, TravelTime]{"train"}, km: 1634.0}
	elcheToFiorentina.edge.edgeCost = TravelTime{cost: elcheToFiorentina.meansOfTransport.TimeBetweenTwoPlaces(elcheToFiorentina)}
	fiorentinaToElche := &RailLine[Place, TravelTime]{edge: &Edge[Place, TravelTime]{from: &fiorentina, to: &elche}, meansOfTransport: &Train[Place, TravelTime]{"train"}, km: 1634.0}
	fiorentinaToElche.edge.edgeCost = TravelTime{cost: fiorentinaToElche.meansOfTransport.TimeBetweenTwoPlaces(fiorentinaToElche)}
	
	aberdeenToDortmund := &RailLine[Place, TravelTime]{edge: &Edge[Place, TravelTime]{from: &aberdeen, to: &dortmund}, meansOfTransport: &Train[Place, TravelTime]{"train"}, km: 1495.0}
	aberdeenToDortmund.edge.edgeCost = TravelTime{cost: aberdeenToDortmund.meansOfTransport.TimeBetweenTwoPlaces(aberdeenToDortmund)}
	dortmundToAberdeen := &RailLine[Place, TravelTime]{edge: &Edge[Place, TravelTime]{from: &dortmund, to: &aberdeen}, meansOfTransport: &Train[Place, TravelTime]{"train"}, km: 1495.0}
	dortmundToAberdeen.edge.edgeCost = TravelTime{cost: dortmundToAberdeen.meansOfTransport.TimeBetweenTwoPlaces(dortmundToAberdeen)}


	AddRailLine(&railMap, aberdeenToBrussel, brusselToAberdeen)
	AddRailLine(&railMap, aberdeenToCannes, cannesToAberdeen)
	AddRailLine(&railMap, brusselToElche, elcheToBrussel)
	AddRailLine(&railMap, cannesToElche, elcheToCannes)
	AddRailLine(&railMap, elcheToFiorentina, fiorentinaToElche)
	AddRailLine(&railMap, aberdeenToDortmund, dortmundToAberdeen)
	
	fmt.Println("Edges:")
	railMap.graph.String()

	path, _ := railMap.graph.ShortestPath(aberdeen, elche)
	fmt.Println("Shortest path from", aberdeen.item.String(), "to", elche.item.String() + ":")
	for _, e := range path {
		fmt.Println(e.from, "->", e.to)
	}

	fmt.Println("\n", railMap.CalculateShortestRoutes(&aberdeen))

	/*
		** OUTPUT **

		Edges:
		Aberdeen -> Brussel Cannes Dortmund 
		Brussel -> Aberdeen Elche 
		Cannes -> Aberdeen Elche 
		Dortmund -> Aberdeen 
		Elche -> Brussel Cannes Fiorentina 
		Fiorentina -> Elche 

		Shortest path from Aberdeen to Elche:
		Aberdeen -> Brussel
		Brussel -> Elche

		Distance from Aberdeen to Aberdeen = 0
		Distance from Aberdeen to Brussel = 14
		Distance from Aberdeen to Cannes = 27
		Distance from Aberdeen to Dortmund = 18
		Distance from Aberdeen to Elche = 36
		Distance from Aberdeen to Fiorentina = 55
	*/

}

func main() {
	RailMapTest()
}

